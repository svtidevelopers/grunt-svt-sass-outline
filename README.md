# grunt-svt-sass-outline

> The best Grunt plugin ever.

## Getting Started
This plugin requires Grunt `~0.4.5`

## The "svt_sass_outline" task

### Overview
In your project's Gruntfile, add a section named `svt_sass_outline` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  svt_sass_outline: {
    options: {
      // Task-specific options go here.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  },
});
```
### Usage Examples

#### Default Options
In this example, the default options are used to do something with whatever. So if the `testing` file has the content `Testing` and the `123` file had the content `1 2 3`, the generated result would be `Testing, 1 2 3.`

```js
grunt.initConfig({
  svt_sass_outline: {
    files: {
      'dest/default_options': ['src/1.scss', 'src/2.scss'],
    },
  },
});
```

## Release History
_(Nothing yet)_
