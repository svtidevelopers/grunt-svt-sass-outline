/*
* grunt-svt-sass-outline
* https://github.com/jakobrahm/grunt-svt-sass-outline
*
* Copyright (c) 2014 Jakob Rahm
* Licensed under the MIT license.
*/

'use strict';

module.exports = function (grunt) {
	var path = require('path');

	grunt.registerMultiTask('svt_sass_outline', '', function () {

		var outline = [];

		this.files.forEach(function (f) {

			f.src.filter(function (filepath) {
				if (!grunt.file.exists(filepath)) {
					grunt.log.warn('Source file "' + filepath + '" not found.');
					return false;
				} else {
					return true;
				}
			}).map(function(filepath) {
				var fObject = {name:path.basename(filepath)}, fProps = [];
				var src = grunt.file.read(filepath);

				var matches = String(src).match(/\$(.*?):(.*?);/g), i = 0;

				if (!matches) {
					return;
				}

				for (; i < matches.length; i++) {
					var match = matches[i];

					var keyVal = match.substr(0, match.length - 1).split(':');
					var value = keyVal[1].trim(), type = '';
					var colorMatch = value.match(/#[0-9a-f]{3,6}|rgba?\([^\)]+\)/gi);

					if (colorMatch && colorMatch.length) {
						type = 'color';
					}

					fProps.push({property: keyVal[0].trim(), value: value, type: type});
				}

				fObject.properties = fProps;
				outline.push(fObject);
			});			

			grunt.file.write(f.dest, JSON.stringify(outline, undefined, '\t'));
			grunt.log.writeln('File "' + f.dest + '" created.');
		});
	});
};
